import sqlite3
import sys
import matplotlib.pyplot as plt

def display(connector,  table):

    fig, ax = plt.subplots(figsize=(5, 5), layout='constrained')

    cur = con.cursor()
    sel = con.cursor()
    exc = cur.execute("SELECT * FROM `vehicles`")

    for car in exc:
        time = []
        speed = []
        for row in sel.execute(f"SELECT * FROM `{table}` WHERE `{table}`.`id` = '" + car[0] + "' ORDER BY `date`"):
            time.append(row[2])
            speed.append(row[1])
        print(car[0])
        ax.plot(time, speed, label=car[0])

    ax.set_xlabel('time')
    ax.set_ylabel(f"{table}")
    ax.set_title(f"{table} of cars")
    ax.legend()
    plt.show()
    con.close()


if __name__ == "__main__":
    con = sqlite3.connect(sys.argv[1])
    display(con, sys.argv[2])



package fr.laguierce.tipe.roads;

import fr.laguierce.tipe.roads.entity.Car;
import fr.laguierce.tipe.roads.entity.CrossRoad;
import fr.laguierce.tipe.roads.entity.Road;
import fr.laguierce.tipe.roads.graphical.UserInterface;
import fr.laguierce.tipe.roads.maths.ListUtil;
import fr.laguierce.tipe.roads.maths.Vector2D;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Simulator {

    public static final long TIME_CONVERSION = 30L;
    public static final int MAX_CARS_AT_TIME = 100_000;
    public static final int MAX_CARS_ALL_TIME = 1_000_000;
    public static final float DISTANCE_THRESHOLD = 3.5f;
    public static final float DISTANCE_CRITIC = 1.5f;
    public static final float SPEED_REDUCTION = 2f/5f;
    public static final float SPEED_GET = 0.02f;

    public static final double WIDTH = 1920;
    public static final double HEIGHT = 1080;
    public static final double ROAD_RADIUS = 6d;
    public static final double CROSS_ROAD_RADIUS = 32d;
    public static final double CAR_RADIUS = 24D;

    private static final List<CrossRoad> CROSS_ROADS = new ArrayList<>();
    private static final List<Road> ROADS = new ArrayList<>();
    private static final List<Car> CARS = new ArrayList<>();
    private static int remainingCar = MAX_CARS_ALL_TIME;

    public static void main(final String... args) {
        CROSS_ROADS.add(new CrossRoad(new Vector2D(ThreadLocalRandom.current().nextDouble(WIDTH), ThreadLocalRandom.current().nextDouble(HEIGHT))));
        for (int i = 0; i < 7; i++) {
            CrossRoad cross = new CrossRoad(new Vector2D(ThreadLocalRandom.current().nextDouble(WIDTH), ThreadLocalRandom.current().nextDouble(HEIGHT)));
            ROADS.add(new Road(ThreadLocalRandom.current().nextInt(250) + 150, cross, CROSS_ROADS.get(ThreadLocalRandom.current().nextInt(CROSS_ROADS.size()))));
            CROSS_ROADS.add(cross);
        }
//        System.out.println("crossRoads = " + crossRoads);
        UserInterface.startInterface(args);
    }

    public static void addNewCar() {
        if (CROSS_ROADS.size() < 2 || remainingCar == 0 || CROSS_ROADS.size() == MAX_CARS_AT_TIME)
            return;
        remainingCar--;
        new Car();
    }

    public static List<CrossRoad> getCrossRoads() {
        return CROSS_ROADS;
    }

    public static List<Road> getRoads() {
        return ROADS;
    }

    public static List<Car> getCars() {
        return List.copyOf(CARS);
    }

    public static void addCar(Car car) {
        CARS.add(car);
    }

    public static void removeCar(Car car) {
        CARS.remove(car);
    }

}

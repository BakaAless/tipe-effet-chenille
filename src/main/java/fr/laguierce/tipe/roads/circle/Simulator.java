package fr.laguierce.tipe.roads.circle;

import fr.laguierce.tipe.roads.circle.entity.Car;
import fr.laguierce.tipe.roads.circle.entity.roads.FakeRoundAbout;
import fr.laguierce.tipe.roads.circle.entity.roads.RoadWay;
import fr.laguierce.tipe.roads.circle.entity.roads.Roundabout;
import fr.laguierce.tipe.roads.circle.graphical.UserInterface;
import fr.laguierce.tipe.roads.circle.logger.VehicleLogger;
import fr.laguierce.tipe.roads.maths.Vector2D;

import java.util.ArrayList;
import java.util.List;

public class Simulator {

    public static VehicleLogger vehicleLogger;

    public static final double WIDTH = 1920;
    public static final double HEIGHT = 1080;

    public final static List<Roundabout> ROUNDABOUTS = new ArrayList<>();

    public static void main(final String... args) {
        vehicleLogger = new VehicleLogger();
        vehicleLogger.start();

        ROUNDABOUTS.add(new Roundabout(new Vector2D(WIDTH / 2, HEIGHT / 2), 350));
        RoadWay roadWayWest = new RoadWay(new FakeRoundAbout(new Vector2D(0, HEIGHT / 2), 1), ROUNDABOUTS.get(0));
        ROUNDABOUTS.get(0).getInput().put(roadWayWest, Math.PI);
        RoadWay roadWayNorth = new RoadWay(new FakeRoundAbout(new Vector2D(WIDTH / 2, 0), 1), ROUNDABOUTS.get(0));
        ROUNDABOUTS.get(0).getInput().put(roadWayNorth, Math.PI / 2);
        RoadWay roadWayEast = new RoadWay(new FakeRoundAbout(new Vector2D(WIDTH, HEIGHT / 2), 1), ROUNDABOUTS.get(0));
        ROUNDABOUTS.get(0).getInput().put(roadWayEast, 0d);
        RoadWay roadWaySouth = new RoadWay(new FakeRoundAbout(new Vector2D(WIDTH / 2, HEIGHT), 1), ROUNDABOUTS.get(0));
        ROUNDABOUTS.get(0).getInput().put(roadWaySouth, 3 * Math.PI / 2);
//        roadWay.addCar(new Car());
        int n = 7;
        for (int i = 0; i < n; i++) {
            Car car = new Car();
            car.angle(2 * i * Math.PI / n + Math.random());
            car.angularSpeed(0);
            ROUNDABOUTS.get(0).addCar(car);
        }
        vehicleLogger.startAck();
        UserInterface.startInterface(args);
    }

    public static synchronized List<Car> getAllCars() {
        return ROUNDABOUTS.stream().flatMap(roundabout -> {
            List<Car> cars = new ArrayList<>(roundabout.getCars());
            cars.addAll(
                    roundabout.getOutput().keySet().stream()
                            .flatMap(road -> road.getCars().stream()).toList()
            );
            cars.addAll(
                    roundabout.getInput().keySet().stream()
                            .flatMap(road -> road.getCars().stream()).toList()
            );
            return cars.stream();
        }).distinct().toList();
    }

}

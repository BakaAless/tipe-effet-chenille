package fr.laguierce.tipe.roads.circle.entity;


import fr.laguierce.tipe.roads.circle.entity.roads.Road;
import fr.laguierce.tipe.roads.circle.entity.roads.RoadWay;
import fr.laguierce.tipe.roads.circle.entity.roads.Roundabout;
import fr.laguierce.tipe.roads.maths.Vector2D;

// Croissements + vitesse max = fct(nb voitures) + anticipation + topographie (: visibilité).
// ralentissement suivant le nombre de véhicules en circulation.
// présentation de résultats graphiques.


// Calcul de la variation de vitesse relative au cours du temps
public class Car extends Entity {

    private double angle;
    private double angularSpeed;
    private double angularAcceleration;
    private double angularSpeedMax;
    private double maxSpeedOffset;
    private boolean paused;

    private Road road;

    public Car() {
        this.angularSpeed = 0.05 + Math.random() / 10;
//        this.maxSpeedOffset = (Math.random() - 0.5) / 100;
    }

    public void update() {
        if (this.paused)
            return;
        this.angularSpeed += this.angularAcceleration;
        this.angularAcceleration /= 10;
        if (this.angularSpeed >= this.angularSpeedMax) {
            this.angularSpeed = this.angularSpeedMax;
            this.angularAcceleration = 0;
        }
        this.angle += this.angularSpeed;
    }

    public double angle() {
        return this.angle;
    }

    public void angle(double angle) {
        this.angle = angle;
    }

    public double angularSpeed() {
        return this.angularSpeed;
    }

    public void angularSpeed(double angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    public double angularSpeedMax() {
        return this.angularSpeedMax;
    }

    public double angularAcc() {
        return this.angularAcceleration;
    }

    public void angularAcc(double angularAcc) {
        this.angularAcceleration = angularAcc;
    }

    public void setRoundabout(Roundabout roundabout) {
        this.road = roundabout;
        this.angularSpeedMax = this.road.getMaxSpeed() + this.maxSpeedOffset;
        this.angularSpeed = Math.max(angularSpeed, angularSpeedMax);
    }

    public void setRoad(Road road) {
        this.road = road;
        this.angularSpeedMax = this.road.getMaxSpeed() + this.maxSpeedOffset;
        this.angularSpeed = Math.max(angularSpeed, angularSpeedMax);
    }

    public Road getRoad() {
        return this.road;
    }

    public Vector2D getVector() {
        if (this.road == null)
            return new Vector2D(0, 0);
        else if (this.road instanceof Roundabout roundabout)
            return new Vector2D(Math.cos(this.angle) * roundabout.getRadius(), Math.sin(this.angle) * roundabout.getRadius());
        else if (this.road instanceof RoadWay roadWay)
            return roadWay.getPosition(this).clone();

        return new Vector2D(0, 0);
    }

    public Vector2D getVelocity() {
        return this.getVector().clone().normalized().multiply(400 * (this.angularSpeed / this.angularSpeedMax + 0.1)).rotate(this.road instanceof Roundabout ? Math.PI / 2 : 0);
    }

    public boolean pause() {
        return this.paused;
    }

    public void pause(boolean paused) {
        this.paused = paused;
    }

}

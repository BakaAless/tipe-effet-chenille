package fr.laguierce.tipe.roads.circle.logger;

import fr.laguierce.tipe.roads.circle.Simulator;

import java.sql.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class VehicleLogger extends Thread {

    private final String name = String.valueOf(System.currentTimeMillis());
    private Connection connection;
    private boolean ready;
    private boolean acking;
    private ScheduledExecutorService service;

    @Override
    public void start() {
        System.out.println("Connecting to a SQLite database");
        this.ready = true;
        try {
            System.out.println("Create connection");
            connection = DriverManager.getConnection("jdbc:sqlite:" + name + ".db");
            connection.setAutoCommit(true);
            System.out.println("Auto-commit TRUE");

            System.out.println("Preparing first table");
            connection.prepareStatement("CREATE TABLE vehicles (id VARCHAR(36));").executeUpdate();
            System.out.println("Preparing second table");
            connection.prepareStatement("CREATE TABLE speed (id VARCHAR(36), speed DOUBLE, date BIGINT);").executeUpdate();
            System.out.println("Preparing third table");
            connection.prepareStatement("CREATE TABLE speed_percent (id VARCHAR(36), percent DOUBLE, date BIGINT);").executeUpdate();
            System.out.println("Preparing fourth table");
            connection.prepareStatement("CREATE TABLE changing_road (id VARCHAR(36), date BIGINT);").executeUpdate();

            System.out.println("Creating thread pool");
            this.service = Executors.newSingleThreadScheduledExecutor();
            System.out.println("Add shutdown hook");
            Runtime.getRuntime().addShutdownHook(new Thread(this::cancel));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean isReady() {
        return this.ready;
    }

    public void startAck() {
        while (!ready) {
            System.out.println("not ready");
        }
        if (this.connection != null) {
            System.out.println("connection not null");
            Simulator.getAllCars().forEach(car -> {
                try {
                    connection.prepareStatement("INSERT INTO vehicles (id) VALUES ('" + car.id + "')").executeUpdate();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
            this.acking = true;
            this.service.scheduleAtFixedRate(new SpeedGetting(this), 0, 10, TimeUnit.MILLISECONDS);
        }
        System.out.println("connection is null");
    }

    public void cancel() {
        this.acking = false;
        try {
            this.connection.close();
            System.out.println("Saved as " + name + ".db");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        this.interrupt();
    }

    private class SpeedGetting implements Runnable {

        private VehicleLogger vehicleLogger;

        public SpeedGetting(VehicleLogger logger) {
            this.vehicleLogger = logger;
        }

        @Override
        public void run() {
            if (!this.vehicleLogger.acking)
                return;
            long date = System.currentTimeMillis();
            Simulator.getAllCars().forEach(car -> {
                try {
                    connection.prepareStatement("INSERT INTO speed (id, speed, date) VALUES ('" + car.id + "', '" + car.angularSpeed() + "', '" + date + "')").executeUpdate();
                    connection.prepareStatement("INSERT INTO speed_percent (id, percent, date) VALUES ('" + car.id + "', '" + car.angularSpeed() / car.angularSpeedMax() + "', '" + date + "')").executeUpdate();
                    // TODO : Calcul variation relative
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }
}

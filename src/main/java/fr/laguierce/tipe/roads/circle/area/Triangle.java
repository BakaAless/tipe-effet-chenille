package fr.laguierce.tipe.roads.circle.area;

import fr.laguierce.tipe.roads.maths.Vector2D;

public class Triangle {

    private Vector2D[] vectors;

    public Triangle(Vector2D point1, Vector2D point2, Vector2D point3) {
        this.vectors = new Vector2D[3];
        this.vectors[0] = point1;
        this.vectors[1] = point2;
        this.vectors[2] = point3;
    }

    public double area() {
        return area(this.vectors[0], this.vectors[1], this.vectors[2]);
    }

    public boolean isInside(Vector2D point) {
        double area1 = area(point, this.vectors[1], this.vectors[2]);
        double area2 = area(this.vectors[0], point, this.vectors[2]);
        double area3 = area(this.vectors[0], this.vectors[1], point);
        return area() == area1 + area2 + area3;
    }

    public static double area(Vector2D point1, Vector2D point2, Vector2D point3) {
        return Math.abs((point1.getX() * (point2.getY() - point3.getY()) + point2.getX() * (point3.getY() - point1.getY()) + point3.getX() * (point1.getY() - point2.getY())) / 2d);
    }


}

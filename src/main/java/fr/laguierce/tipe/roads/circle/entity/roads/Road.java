package fr.laguierce.tipe.roads.circle.entity.roads;

import fr.laguierce.tipe.roads.circle.entity.Car;
import fr.laguierce.tipe.roads.circle.entity.Entity;

import java.util.ArrayList;
import java.util.List;

public abstract class Road extends Entity {

    double maxSpeed = 0.1;
    List<Car> cars = new ArrayList<>();

    public double getMaxSpeed() {
        return this.maxSpeed;
    }
}

package fr.laguierce.tipe.roads.circle.entity.roads;

import fr.laguierce.tipe.roads.circle.entity.Car;
import fr.laguierce.tipe.roads.maths.Vector2D;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Roundabout extends FakeRoundAbout {

    private List<Road> connectedRoads;
    private Map<RoadWay, Double> input;
    private Map<RoadWay, Double> output;

    public Roundabout(Vector2D center, double radius) {
        super(center, radius);
        this.cars = new ArrayList<>();
        this.connectedRoads = new ArrayList<>();
        this.input = new HashMap<>();
        this.output = new HashMap<>();
    }


    public Vector2D getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public synchronized List<Car> getCars() {
        return cars;
    }

    public void addCar(Car car) {
        car.setRoundabout(this);
        this.cars.add(car);
    }

    public List<Road> getConnectedRoads() {
        return connectedRoads;
    }

    public Map<RoadWay, Double> getInput() {
        return this.input;
    }

    public Map<RoadWay, Double> getOutput() {
        return this.output;
    }
}

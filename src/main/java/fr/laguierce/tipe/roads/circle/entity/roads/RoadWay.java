package fr.laguierce.tipe.roads.circle.entity.roads;

import fr.laguierce.tipe.roads.circle.entity.Car;
import fr.laguierce.tipe.roads.maths.Vector2D;

import java.util.List;

public class RoadWay extends Road {

    private FakeRoundAbout input;
    private FakeRoundAbout output;

    public RoadWay(FakeRoundAbout input, FakeRoundAbout output) {
        this.input = input;
        this.output = output;
    }

    public Vector2D getPosition(Car car) {
        return this.entryPoint().multiply(1 - car.angle() / 30).add(this.exitPoint().multiply(car.angle() / 30));
    }

    public Vector2D entryPoint() {
        if (input == null)
            return new Vector2D(0, 0);
        if (input instanceof Roundabout roundabout) {
            double theta = roundabout.getOutput().get(this);
            return new Vector2D(Math.cos(theta), Math.sin(theta)).multiply(input.radius).add(roundabout.getCenter());
        } else
            return input.center.clone();
    }

    public Vector2D exitPoint() {
        if (output == null)
            return new Vector2D(0, 0);
        if (output instanceof Roundabout roundabout) {
            double theta = roundabout.getInput().get(this);
            return new Vector2D(Math.cos(theta), Math.sin(theta)).multiply(output.radius).add(roundabout.getCenter());
        } else
            return output.center.clone();
    }

    public void addCar(Car car) {
        car.setRoad(this);
        this.cars.add(car);
    }

    public List<Car> getCars() {
        return this.cars;
    }

    public FakeRoundAbout getInput() {
        return this.input;
    }

    public FakeRoundAbout getOutput() {
        return this.output;
    }
}

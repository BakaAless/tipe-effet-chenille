package fr.laguierce.tipe.roads.circle.entity.roads;

import fr.laguierce.tipe.roads.maths.Vector2D;

public class FakeRoundAbout extends Road {

    protected Vector2D center;
    protected double radius;

    public FakeRoundAbout(Vector2D center, double radius) {
        this.center = center;
        this.radius = radius;
    }

}

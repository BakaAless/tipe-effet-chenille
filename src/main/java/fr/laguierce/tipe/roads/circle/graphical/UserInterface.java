package fr.laguierce.tipe.roads.circle.graphical;

import fr.laguierce.tipe.roads.circle.Simulator;
import fr.laguierce.tipe.roads.circle.area.Triangle;
import fr.laguierce.tipe.roads.circle.entity.Car;
import fr.laguierce.tipe.roads.circle.entity.roads.Roundabout;
import fr.laguierce.tipe.roads.maths.Vector2D;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;

public class UserInterface extends Application {

    public static void startInterface(final String... args) {
        launch(args);
    }

    private Scene scene;
    private GraphicsContext context;
    double x, y;
    boolean pause;

    @Override
    public void start(Stage primaryStage) throws Exception {
        final StackPane pane = new StackPane();

        final Canvas canvas = new Canvas(Simulator.WIDTH, Simulator.HEIGHT);

        pane.getChildren().add(canvas);

        primaryStage.setScene(new Scene(pane, Simulator.WIDTH, Simulator.HEIGHT));
        primaryStage.setTitle("Simulation routes");
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.centerOnScreen();

        this.context = canvas.getGraphicsContext2D();
//        this.context.translate(Simulator.WIDTH / 2, Simulator.HEIGHT / 2);
//        this.context.scale(0.5, 0.5);

        final Timeline timeLine = new Timeline(new KeyFrame(Duration.millis(20), event -> this.render()));
        timeLine.setCycleCount(Timeline.INDEFINITE);
        timeLine.play();

        primaryStage.setOnCloseRequest(event -> {
            timeLine.stop();
            System.exit(0);
        });
//
//        canvas.setOnMouseEntered(event -> {
//            this.pause = false;
//        });
//        canvas.setOnMouseExited(event -> {
//            this.pause = true;
//        });
    }

    int tick = 0;

    public void render() {
        if (this.pause)
            return;
        this.context.clearRect(0, 0, Simulator.WIDTH, Simulator.HEIGHT);
//        this.context.setFill(map(tick % 2));/DTH, Simulator.HEIGHT);

        this.context.setStroke(Color.BLACK);
//        this.context.rotate(tick / 25f);

        Simulator.ROUNDABOUTS.forEach(roundabout -> {
            Vector2D roundaboutPosition = roundabout.getCenter();
            int radiusCar = 50;

            roundabout.getInput().forEach((road, theta) -> {
                Vector2D entry = road.entryPoint();
                Vector2D exit = road.exitPoint();
                this.context.strokeLine(entry.getX(), entry.getY(), exit.getX(), exit.getY());
                new ArrayList<>(road.getCars()).forEach(car -> {
                    car.update();
                    Vector2D position = car.getVector();
                    Vector2D rotate = car.getVelocity();
                    Vector2D point1 = rotate.clone().rotate(Math.PI / 6).add(position);
                    Vector2D point2 = rotate.clone().rotate(- Math.PI / 6).multiply(1.2).add(position);


                    this.context.strokeLine(point1.getX(), point1.getY(), point2.getX(), point2.getY());
                    this.context.strokeLine(point1.getX(), point1.getY(), position.getX(), position.getY());
                    this.context.strokeLine(point2.getX(), point2.getY(), position.getX(), position.getY());
                    this.context.setFill(Color.color(0, 0, 0, 0.25));
                    this.context.fillPolygon(new double[] {position.getX(), point1.getX(), point2.getX()}, new double[] {position.getY(), point1.getY(), point2.getY()}, 3);

                    this.context.setFill(map(car.pause() ? 0 : car.angularSpeed() / 0.06));

                    this.context.fillOval(position.getX() - radiusCar / 2d, position.getY() - radiusCar / 2d, radiusCar, radiusCar);
                    this.context.strokeOval(position.getX() - radiusCar / 2d, position.getY() - radiusCar / 2d, radiusCar, radiusCar);

//                this.context.fillOval(point1.getX() - radiusCar / 2d, point1.getY() - radiusCar / 2d, radiusCar, radiusCar);
//                this.context.fillOval(point2.getX() - radiusCar / 2d, point2.getY() - radiusCar / 2d, radiusCar, radiusCar);
                    // check if a car is inside the triangle made of those three points

                    for (Car user : road.getCars()) {
                        if (user != car) {
                            if (new Triangle(position, point1, point2).isInside(user.getVector())) {
//                            car.angularSpeed(Math.max(car.angularSpeed() - user.angularSpeed() * (0.01 + 1 / Math.abs((user.angle() - car.angle()) % Math.PI)), 0));
                                car.angularSpeed(Math.max(0, car.angularSpeed() - 0.5 / (car.getVector().remove(user.getVector()).lengthSquared())));
                                car.angularAcc(0);
                                break;
                            }
                            else {
                                car.angularAcc(0.0001);
                            }
                        }
                    }
                    for (Car user : roundabout.getCars()) {
                        if (user != car) {
                            if (new Triangle(position, point1, point2).isInside(user.getVector())) {
//                            car.angularSpeed(Math.max(car.angularSpeed() - user.angularSpeed() * (0.01 + 1 / Math.abs((user.angle() - car.angle()) % Math.PI)), 0));
                                car.angularSpeed(Math.max(0, car.angularSpeed() - 0.5 / (car.getVector().remove(user.getVector()).lengthSquared())));
                                car.angularAcc(0);
                                break;
                            }
                            else {
                                car.angularAcc(0.0003);
                            }
                        }
                    }

                    if (position.clone().subtract(exit).lengthSquared() < 0.5) {
                        if (road.getOutput() instanceof Roundabout roundabout2) {
                            roundabout2.addCar(car);
                            road.getCars().remove(car);
                            car.angle(theta);
                        }
                    }


                });
            });
            this.context.translate(roundaboutPosition.getX(), roundaboutPosition.getY());
            this.context.setFill(Color.WHITE);
            this.context.fillOval(- roundabout.getRadius(), - roundabout.getRadius(), 2 * roundabout.getRadius(), 2 * roundabout.getRadius());
            this.context.strokeOval(- roundabout.getRadius(), - roundabout.getRadius(), 2 * roundabout.getRadius(), 2 * roundabout.getRadius());

            roundabout.getCars().forEach(car -> {
                if (roundabout.getCars().size() > 7) {
                }
                car.update();
                Vector2D position = car.getVector();
                Vector2D rotate = car.getVelocity().rotate(Math.PI/6);
                Vector2D point1 = rotate.clone().rotate(Math.PI / 6).add(position);
                Vector2D point2 = rotate.clone().rotate(- Math.PI / 5).multiply(1.2).add(position);


                this.context.strokeLine(point1.getX(), point1.getY(), point2.getX(), point2.getY());
                this.context.strokeLine(point1.getX(), point1.getY(), position.getX(), position.getY());
                this.context.strokeLine(point2.getX(), point2.getY(), position.getX(), position.getY());
                this.context.setFill(Color.color(0, 0, 0, 0.25));
                this.context.fillPolygon(new double[] {position.getX(), point1.getX(), point2.getX()}, new double[] {position.getY(), point1.getY(), point2.getY()}, 3);

                this.context.setFill(map(car.pause() ? 0 : car.angularSpeed() / 0.06));

                this.context.fillOval(position.getX() - radiusCar / 2d, position.getY() - radiusCar / 2d, radiusCar, radiusCar);
                this.context.strokeOval(position.getX() - radiusCar / 2d, position.getY() - radiusCar / 2d, radiusCar, radiusCar);

//                this.context.fillOval(point1.getX() - radiusCar / 2d, point1.getY() - radiusCar / 2d, radiusCar, radiusCar);
//                this.context.fillOval(point2.getX() - radiusCar / 2d, point2.getY() - radiusCar / 2d, radiusCar, radiusCar);
                // check if a car is inside the triangle made of those three points

                for (Car user : roundabout.getCars()) {
                    if (user != car) {
                        if (new Triangle(position, point1, point2).isInside(user.getVector())) {
//                            car.angularSpeed(Math.max(car.angularSpeed() - user.angularSpeed() * (0.01 + 1 / Math.abs((user.angle() - car.angle()) % Math.PI)), 0));
                            car.angularSpeed(Math.max(0, car.angularSpeed() - 0.75 / (car.getVector().remove(user.getVector()).lengthSquared())));
                            car.angularAcc(0);
                            break;
                        }
                        else {
                            car.angularAcc(0.0001);
                        }
                    }
                }

            });

            this.context.translate(- roundaboutPosition.getX(), - roundaboutPosition.getY());
        });

        tick++;
    }

    public Color map(double speed) {
        if (speed > 1)
            speed = 1;
        return Color.color((1 - speed) * 0.8627451f, speed * 0.5019608f + (1 - speed) * 0.078431375f, (1 - speed) * 0.23529412f);
    }

}

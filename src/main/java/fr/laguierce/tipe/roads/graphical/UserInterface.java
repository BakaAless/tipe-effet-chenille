package fr.laguierce.tipe.roads.graphical;

import fr.laguierce.tipe.roads.*;
import fr.laguierce.tipe.roads.entity.Car;
import fr.laguierce.tipe.roads.entity.CrossRoad;
import fr.laguierce.tipe.roads.entity.Road;
import fr.laguierce.tipe.roads.maths.MathUtils;
import fr.laguierce.tipe.roads.maths.Vector2D;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Point3D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class UserInterface extends Application {

    public static void startInterface(final String... args) {
        launch(args);
    }

    private Scene scene;
    private GraphicsContext context;

    @Override
    public void start(Stage primaryStage) throws Exception {
        final StackPane pane = new StackPane();

        final Canvas canvas = new Canvas(Simulator.WIDTH, Simulator.HEIGHT);

        pane.getChildren().add(canvas);

        primaryStage.setScene(new Scene(pane, Simulator.WIDTH, Simulator.HEIGHT));
        primaryStage.setTitle("Simulation routes");
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.centerOnScreen();

        this.context = canvas.getGraphicsContext2D();

        final Timeline timeLine = new Timeline(new KeyFrame(Duration.millis(25), event -> this.render()));
        timeLine.setCycleCount(Timeline.INDEFINITE);
        timeLine.play();

        primaryStage.setOnCloseRequest(event -> timeLine.stop());
    }

    int tick = 0;

    public void render() {
        tick++;
        if (tick % 5 == 0)
            Simulator.addNewCar();
        Simulator.getCars().forEach(Car::update);

//        this.context.setFill(Color.WHITE);
        this.context.clearRect(0, 0, Simulator.WIDTH, Simulator.HEIGHT);

        this.context.setFill(Color.BLACK);
//        this.context.fillRect(Simulator.WIDTH / 2, Simulator.HEIGHT / 2, 16, 16);
        this.context.setFill(Color.BLACK);
        this.context.setLineWidth(Simulator.ROAD_RADIUS);
        this.context.setLineCap(StrokeLineCap.BUTT);

//        this.context.getCanvas().setRotate(Math.PI * tick);
//        this.context.getCanvas().setTranslateX(ThreadLocalRandom.current().nextInt((int) Simulator.WIDTH));
//        this.context.getCanvas().setTranslateX(ThreadLocalRandom.current().nextInt((int) Simulator.HEIGHT));
        for (Road road : Simulator.getRoads()) {
            List<CrossRoad> alreadyTreated = new ArrayList<>();
            for (CrossRoad start : road.getCrossRoads()) {
                alreadyTreated.add(start);
                for (CrossRoad end : road.getCrossRoads()) {
                    if (alreadyTreated.contains(end))
                        continue;
                    if (!road.getBezierCurves().containsKey(start.hashCode() + end.hashCode()))
                        road.getBezierCurves().put(start.hashCode() + end.hashCode(), MathUtils.drawBezierCurvesVectors(start.getPosition(), end.getPosition()));
                    List<Vector2D> positions = new ArrayList<>();
                    for (double lambda = 0; lambda <= 1; lambda += 0.002)
                        positions.add(MathUtils.drawBezierCurvesPoint(road.getBezierCurves().get(start.hashCode() + end.hashCode()), lambda));
                    for (int i = 0; i < positions.size() - 1; i++)
                        this.context.strokeLine(positions.get(i).getX(), positions.get(i).getY(), positions.get(i + 1).getX(), positions.get(i + 1).getY());
                }
            }
        }
        for (Road road : Simulator.getRoads()) {
            this.context.setFill(Color.RED);
            for (CrossRoad crossRoad : road.getCrossRoads()) {
                this.context.fillOval(crossRoad.getPosition().getX() - Simulator.CROSS_ROAD_RADIUS / 2, crossRoad.getPosition().getY() - Simulator.CROSS_ROAD_RADIUS / 2, Simulator.CROSS_ROAD_RADIUS, Simulator.CROSS_ROAD_RADIUS);
            }
        }
        for (Road road : Simulator.getRoads()) {
            for (Car car : road.getCars()) {
                try {
                    Vector2D position = MathUtils.drawBezierCurvesPoint(road.getBezierCurves().get(car.getLastCrossRoad().hashCode() + car.getTarget().hashCode()), car.getCurrentStep() / (car.getRelativeStepToReach() + 0D));
                    this.context.setFill(Color.LIMEGREEN);
                    this.context.fillOval(position.getX() - Simulator.CAR_RADIUS / 2, position.getY() - Simulator.CAR_RADIUS / 2, Simulator.CAR_RADIUS, Simulator.CAR_RADIUS);
                } catch (Exception e) {
                    System.out.println("car = " + car);
                    throw new RuntimeException(e);
                }
            }
        }
    }

}

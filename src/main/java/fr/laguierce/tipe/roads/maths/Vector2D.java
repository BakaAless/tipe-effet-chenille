package fr.laguierce.tipe.roads.maths;

public class Vector2D implements Cloneable {

    private double x, y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D set(Vector2D other) {
        this.x = other.x;
        this.y = other.y;
        return this;
    }

    public Vector2D set(Vector2D other, double max) {
        this.x = Math.min(other.x, max);
        this.y = Math.min(other.y, max);
        return this;
    }

    public Vector2D set(Vector2D other, double min, double max) {
        this.x = Math.max(Math.min(other.x, max), -max);
        this.y = Math.max(Math.min(other.y, max), -max);
        return this;
    }

    public Vector2D set(double angle) {
        this.x = Math.cos(angle);
        this.y = Math.sin(angle);
        return this;
    }

    public double getX() {
        return this.x;
    }

    public Vector2D setX(double x) {
        this.x = x;
        return this;
    }

    public double getY() {
        return this.y;
    }

    public Vector2D setY(double y) {
        this.y = y;
        return this;
    }

    public Vector2D add(double x, double y) {
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector2D add(double x, double y, double max) {
        this.x += x;
        this.x = Math.min(Math.abs(this.x), Math.abs(max)) * Math.signum(this.x);
        this.y += y;
        this.y = Math.min(Math.abs(this.y), Math.abs(max)) * Math.signum(this.y);
        return this;
    }

    public Vector2D add(Vector2D other) {
        this.x += other.x;
        this.y += other.y;
        return this;
    }

    public Vector2D remove(Vector2D other) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    public Vector2D add(Vector2D other, int max) {
        this.x += Math.min(other.x, max);
        this.y += Math.max(other.y, max);
        return this;
    }

    public Vector2D subtract(Vector2D other) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }

    public Vector2D multiply(double factor) {
        this.x *= factor;
        this.y *= factor;
        return this;
    }

    private double constrainToRange(double angle, double min, double max) {
        return Math.min(Math.max(angle, min), max);
    }

    public double dot(Vector2D other) {
        return x * other.x + y * other.y;
    }

    public double angle(Vector2D other) {
        double dot = dot(other) / (lengthSquared() * other.lengthSquared());
        return Math.acos(dot);
    }

    public Vector2D rotate(angle) {
        double angleCos = Math.cos(angle);
        double angleSin = Math.sin(angle);
        double x = angleCos * getX() - angleSin * getY();
        double y = angleSin * getX() + angleCos * getY();
        return new Vector2D(x, y);
    }

    public Vector2D normalized()  {
        double length = lengthSquared();
        this.x /= length;
        this.y /= length;
        return this;
    }

    public double length() {
        return Math.pow(this.x, 2) + Math.pow(this.y, 2);
    }

    public double lengthSquared() {
        return Math.sqrt(this.length());
    }

    public Vector2D minimum(Vector2D vector) {
        return new Vector2D(Math.min(this.x, vector.x), Math.min(this.y, vector.y));
    }

    public Vector2D maximum(Vector2D vector) {
        return new Vector2D(Math.max(this.x, vector.x), Math.max(this.y, vector.y));
    }

    @Override
    public Vector2D clone() {
        try {
            return (Vector2D) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Vector2D vector) {
            return this.x == vector.x && this.y == vector.y;
        }
        return false;
    }
}

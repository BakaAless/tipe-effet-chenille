package fr.laguierce.tipe.roads.maths;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ListUtil {

    public static <T> T random(List<T> list) {
        if (list.size() == 0)
            return null;
        return list.get(ThreadLocalRandom.current().nextInt(list.size()));
    }

}

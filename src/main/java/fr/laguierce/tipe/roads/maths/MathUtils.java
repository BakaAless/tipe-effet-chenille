package fr.laguierce.tipe.roads.maths;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MathUtils {

    public static List<Vector2D> drawBezierCurvesVectors(Vector2D start, Vector2D end) {
        Random random = new Random(start.hashCode() + end.hashCode());
        Vector2D direction = end.clone().remove(start)
                .normalized()
                .rotate(Math.PI / 2D * (random.nextDouble(1) > 0.5 ? 1 : -1))
                .multiply((int) end.clone().remove(start).length() % 270);
        Vector2D thirdPoint = start.clone().add(direction);
        Vector2D fourPoint = end.clone().add(direction.multiply(Math.random() > 0.5 ? -1 : 1));
        return List.of(start, thirdPoint, fourPoint, end);
    }

    public static Vector2D drawBezierCurvesPoint(List<Vector2D> nodes, double step) {
        List<Vector2D> points = new ArrayList<>();
//        if (nodes == null)
//            return new Vector2D(0, 0);
        while (nodes.size() > 1) {
            for (int i = 0; i < nodes.size() - 1; i++)
                points.add(lerp(nodes.get(i), nodes.get(i + 1), step));
            nodes = points;
            points = new ArrayList<>();
        }
        return nodes.get(0);
    }

    public static Vector2D lerp(final Vector2D start, final Vector2D end, double step) {
        return new Vector2D(step * start.getX() + (1 - step) * end.getX(), step * start.getY() + (1 - step) * end.getY());
    }

}

package fr.laguierce.tipe.roads.entity;

import fr.laguierce.tipe.roads.maths.Vector2D;

import java.util.ArrayList;
import java.util.List;

public class CrossRoad extends Entity {

    private final Vector2D position;
    private final List<Road> roads;

    public CrossRoad(Vector2D position) {
        this.position = position;
        this.roads = new ArrayList<>();
    }

    public Vector2D getPosition() {
        return position;
    }

    public List<Road> getRoads() {
        return this.roads;
    }

    @Override
    public String toString() {
        return "CrossRoad{" +
                "position=" + position +
                ",roads=" + roads +
                "}";
    }
}

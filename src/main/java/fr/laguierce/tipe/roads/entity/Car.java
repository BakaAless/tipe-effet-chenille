package fr.laguierce.tipe.roads.entity;

import fr.laguierce.tipe.roads.Simulator;
import fr.laguierce.tipe.roads.maths.ListUtil;

import java.util.*;

public class Car extends Entity {

    private int tick;
    private float speed;
    private float acceleration;
    private float currentStep;
    private Road currentRoad;
    private final Deque<Road> previousRoads;
    private final Deque<CrossRoad> crossRoads;
    private CrossRoad target;

    public Car() {
        this.tick = 0;
        this.speed = 0.5f;
        this.currentStep = 0f;
        this.previousRoads = new ArrayDeque<>();
        this.crossRoads = new ArrayDeque<>();
        this.crossRoads.add(ListUtil.random(Simulator.getCrossRoads()));
        this.currentRoad = ListUtil.random(this.crossRoads.peekLast().getRoads());
        this.currentRoad.getCars().add(this);
        this.target = this.currentRoad.getCrossRoads().stream().filter(crossRoad -> crossRoad != crossRoads.peekLast()).findAny().get();
        Simulator.addCar(this);
    }

    public void update() {
        if (this.removed) {
            Simulator.removeCar(this);
            return;
        }
        this.tick++;
        // TODO : check if a car is too close from athen add less than one to currentStep
        this.currentRoad.getCars().stream()
                .filter(cars -> cars != this && cars.getCurrentStep() > this.getCurrentStep() && Math.abs(cars.getCurrentStep() - this.getCurrentStep()) < Simulator.DISTANCE_THRESHOLD)
                .findFirst()
                .ifPresentOrElse(car -> {
                            if (Math.abs(car.getCurrentStep() - this.getCurrentStep()) < Simulator.DISTANCE_CRITIC) {
                                this.acceleration -= 1 / Math.abs(car.getCurrentStep() - this.getCurrentStep());
                                return;
                            }
                            this.acceleration -= 1 / Math.abs(car.getCurrentStep() - this.getCurrentStep()) * (1 - Simulator.SPEED_REDUCTION);
                        },
                        () -> {
                            if (this.speed == this.currentRoad.getMaxSpeed()) {
                                this.acceleration = 0;
                                return;
                            }
                            this.acceleration = Simulator.SPEED_GET;
                        });
        if (Math.random() > 0.99) {
            this.speed *= (1 - Simulator.SPEED_REDUCTION);
        }
        this.speed = Math.max(0, Math.min(this.speed + this.acceleration, this.currentRoad.getMaxSpeed()));
        this.currentStep += this.speed;
        if (this.currentRoad == null)
            this.remove();
        if (this.currentStep >= this.currentRoad.getRelativeLength()) {
            this.previousRoads.add(this.currentRoad);
            System.out.println("Car (" + this.id + ") has traveled from (" + this.crossRoads.peekFirst().id + ") to (" + this.target.id + ") via (" + this.currentRoad.id + ") in " + this.tick + " ticks <=> " + this.tick * Simulator.TIME_CONVERSION + " seconds");
            this.nextRoad();
        }
    }

    private void nextRoad() {
        this.currentStep = 0;
        this.currentRoad.getCars().remove(this);
        if (Math.random() > 0.9) {
            this.remove();
            return;
        }
        this.crossRoads.add(this.target);
        this.currentRoad = ListUtil.random(this.crossRoads.peekFirst().getRoads());
        if (this.currentRoad == null)
            return;
        Optional<CrossRoad> next = this.currentRoad.getCrossRoads().stream().filter(crossRoad -> crossRoad != this.target && crossRoad != this.crossRoads.peekFirst()).findAny();
        if (next.isPresent()) {
            this.currentRoad.getCars().add(this);
            this.target = next.get();
        } else {
            this.remove();
        }
    }

    public void remove() {
        this.removed = true;
        Simulator.removeCar(this);
        System.out.println("Removing car (" + this.id + ") after " + this.tick + " ticks <=> " + this.tick * Simulator.TIME_CONVERSION + " seconds");
    }

    public CrossRoad getLastCrossRoad() {
        return this.crossRoads.peekFirst();
    }

    public CrossRoad getTarget() {
        return this.target;
    }

    public float getCurrentStep() {
        return this.currentStep;
    }

    public int getStepToReach() {
        return this.currentRoad.getLength();
    }

    public int getRelativeStepToReach() {
        return this.currentRoad.getRelativeLength();
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getSpeed() {
        return this.speed;
    }

}

package fr.laguierce.tipe.roads.entity;

import fr.laguierce.tipe.roads.maths.Vector2D;

import java.util.*;

public class Road extends Entity {

    private final List<CrossRoad> crossRoads;
    private int length;
    private float maxSpeed;
    private final List<Car> cars;
    private final Map<Integer, List<Vector2D>> bezierVectors;

    public Road(List<CrossRoad> crossRoads, int length) {
        this.crossRoads = crossRoads;
        this.crossRoads.forEach(crossRoad -> crossRoad.getRoads().add(this));
//        System.out.println("crossRoads = " + crossRoads);
        this.length = length;
        this.maxSpeed = 1f;
        this.cars = new LinkedList<>();
        this.bezierVectors = new HashMap<>();
    }

    public Road(int length, CrossRoad... crossRoads) {
        this(Arrays.asList(crossRoads), length);
    }

    public List<CrossRoad> getCrossRoads() {
        return crossRoads;
    }

    public int getLength() {
        return this.length;
    }

    public float getMaxSpeed() {
        return this.maxSpeed;
    }

    public int getRelativeLength() {
        return length + cars.size();
    }

    public List<Car> getCars() {
        return this.cars;
    }

    public Map<Integer, List<Vector2D>> getBezierCurves() {
        return this.bezierVectors;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

package fr.laguierce.tipe.roads.entity;

import java.util.UUID;

public abstract class Entity {

    protected final UUID id = UUID.randomUUID();
    protected boolean removed = false;

    public UUID getId() {
        return this.id;
    }

}
